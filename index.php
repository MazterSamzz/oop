<?php
    require_once "animal.php";
    require_once "Ape.php";
    require_once "Frog.php";

    $sheep = new Animal("shaun");
    echo "Nama : ".$sheep->name."<br>"; // "shaun"
    echo "Berkaki : ".$sheep->legs."<br>"; //2
    echo "Berdarah dingin :".$sheep->cold_blooded."<br>";
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama : ".$sungokong->name."<br>";
    echo "Berkaki : ".$sungokong->legs."<br>";
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";
    $kodok = new Frog("buduk");
    echo "Nama : ".$kodok->name."<br>";
    echo "Berkaki : ".$kodok->legs."<br>";
    $kodok->jump() ; // "hop hop"

?>