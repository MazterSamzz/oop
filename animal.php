<?php
    class Animal{
        public  $legs = 2,
                $cold_blooded = "false";

        function __construct($name) {
            $this->name = $name; 
         }

        function get_name() {
            return $this->name;
        }

        function get_legs(){
            return $this->legs;
        }
        function get_cold_blooded(){
            echo $this->cold_blooded;
        }
        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
        
    }